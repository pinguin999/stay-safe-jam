#include "GameObject.hpp"
#include <Box2D/Box2D.h>
#include <jngl.hpp>

class Helth : public GameObject {
public:
	Helth(b2World& world, jngl::Vec2 position);
	~Helth();


	void draw() const override;
	bool step() override;
    void onContact(GameObject*) override;

private:
	jngl::Sprite sprite{"helth"};
    bool destroy = false;
};
