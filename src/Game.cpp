#include "Game.hpp"

#include "constants.hpp"
#include "engine/Animation.hpp"
#include "engine/Fade.hpp"
#include "engine/Sprite.hpp"
#include "GameObject.hpp"
#include "Player.hpp"
#include "House.hpp"
#include "ToiletPaper.hpp"
#include "Virus.hpp"
#include "helth.hpp"

#include <cmath>
#include <jngl.hpp>

Game::Game()
: world({ 0, 0 }),
  groundPosition(pixelToMeter({ 0, GROUND + PIXEL_PER_METER })) {

	world.SetContactListener(&contactListener);
	b2BodyDef bodyDef;
	bodyDef.position = groundPosition;
	bodyDef.type = b2_kinematicBody;
	ground = world.CreateBody(&bodyDef);

	b2PolygonShape shape;
	shape.SetAsBox(BOUNDS_W / PIXEL_PER_METER, 1);
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.7f;
	fixtureDef.restitution = 0.1f;
	fixtureDef.filter.categoryBits = FILTER_CATEGORY_SOLID_OBJECT;
	fixtureDef.filter.maskBits = 0xffff;
	ground->CreateFixture(&fixtureDef);

	int streets[4] = {-330, -100, 100, 330};
	for(int i = 0; i < 4; i++) {
		// oben rechts
		gameObjects.emplace_back(std::make_shared<House>(world, jngl::Vec2(-650, streets[i])));
		gameObjects.emplace_back(std::make_shared<House>(world, jngl::Vec2(-550, streets[i])));
		gameObjects.emplace_back(std::make_shared<House>(world, jngl::Vec2(-450, streets[i])));

		// oben mitte
		gameObjects.emplace_back(std::make_shared<House>(world, jngl::Vec2(-200, streets[i])));
		gameObjects.emplace_back(std::make_shared<House>(world, jngl::Vec2(-100, streets[i])));
		gameObjects.emplace_back(std::make_shared<House>(world, jngl::Vec2(0, streets[i])));
		gameObjects.emplace_back(std::make_shared<House>(world, jngl::Vec2(100, streets[i])));
		gameObjects.emplace_back(std::make_shared<House>(world, jngl::Vec2(200, streets[i])));
		gameObjects.emplace_back(std::make_shared<House>(world, jngl::Vec2(300, streets[i])));
		gameObjects.emplace_back(std::make_shared<House>(world, jngl::Vec2(400, streets[i])));

		// oben rechts
		gameObjects.emplace_back(std::make_shared<House>(world, jngl::Vec2(650, streets[i])));
	}

	gameObjects.emplace_back(std::make_shared<Player>(world, jngl::Vec2(-300, 300), 1));



	// gameObjects.emplace_back(std::make_shared<ToiletPaper>(world, jngl::Vec2(-250, 250)));
	// gameObjects.emplace_back(std::make_shared<ToiletPaper>(world, jngl::Vec2(-300, 200)));
	// gameObjects.emplace_back(std::make_shared<Virus>(world, jngl::Vec2(300, -200)));
	// gameObjects.emplace_back(std::make_shared<Virus>(world, jngl::Vec2(100, -340)));
	// gameObjects.emplace_back(std::make_shared<Virus>(world, jngl::Vec2(120, -500)));
	// gameObjects.emplace_back(std::make_shared<Virus>(world, jngl::Vec2(250, -180)));
	// gameObjects.emplace_back(std::make_shared<Virus>(world, jngl::Vec2(170, -360)));
	// gameObjects.emplace_back(std::make_shared<Virus>(world, jngl::Vec2(90, -260)));
	// gameObjects.emplace_back(std::make_shared<Virus>(world, jngl::Vec2(400, -220)));


	// gameObjects.emplace_back(std::make_shared<Helth>(world, jngl::Vec2(500, -220)));

	jngl::loop("sfx/song.ogg");
}

Game::~Game() = default;

void Game::onLoad() {
	jngl::setMouseVisible(false);
}

void Game::step() {

	placeObject();

	won = true;
	for (auto it = gameObjects.begin(); it != gameObjects.end(); ++it)
	{
		if (const auto house = dynamic_cast<House*>(&*(*it))) {
			if (!house->happy)
				won = false;
				break;
		}
	}

	world.Step((1.f / 60.f), 8, 3);

	for (auto it = gameObjects.begin(); it != gameObjects.end(); ++it) {
		if ((*it)->step() || (*it)->getPosition().y > 1000) {
			it = gameObjects.erase(it);
			if (it == gameObjects.end()) {
				break;
			}
		}
	}
	for (auto it = animations.begin(); it != animations.end(); ++it) {
		if ((*it)->step()) {
			it = animations.erase(it);
			if (it == animations.end()) {
				break;
			}
		}
	}
	for (auto& sprite : sprites) {
		sprite->step();
	}
	for (auto& sprite : needToRemove) {
		sprites.erase(std::remove_if(
		                  sprites.begin(), sprites.end(),
		                  [sprite](const std::unique_ptr<Sprite>& p) { return p.get() == sprite; }),
		              sprites.end());
	}
	needToRemove.clear();
}

void Game::draw() const {
	background.draw();
	for (auto& gameObject : gameObjects) {
		gameObject->draw();
	}
	for (auto& animation : animations) {
		animation->draw();
	}
	for (auto& sprite : sprites) {
		sprite->draw();
	}

	if (won)
	{
		jngl::setFontSize(70);
		jngl::print("YOU WON!", {0, 0});
	}
}

b2World& Game::getWorld() {
	return world;
}

void Game::addSprite(Sprite* sprite) {
	sprites.emplace_back(sprite);
}

void Game::removeSprite(Sprite* sprite) {
	needToRemove.insert(sprite);
}

jngl::Vec2 Game::getStreetPos()
{
	int street = (rand() % 4) + 1;

	switch (street)
	{
	case 1:
		return jngl::Vec2(((rand() % 1300) - 650) , -((rand() % 70) + 200));
		break;

	case 2:
		return jngl::Vec2(((rand() % 1300) - 650) , ((rand() % 70) + 180));
		break;

	case 3:
		return jngl::Vec2(((rand() % 100) - 400) , ((rand() % 700) - 350));
		break;

	case 4:
		return jngl::Vec2(((rand() % 100) + 470) , ((rand() % 700) - 350));
		break;

	default:
		break;
	}
	return jngl::Vec2(0, 0);

}


void Game::placeObject()
{
	int obj = (rand() % 200) + 1;

	jngl::Vec2 pos = getStreetPos();
	if (jngl::Vec2(0, 0) == pos)
		return;

	switch (obj)
	{
	case 1:
		gameObjects.emplace_back(std::make_shared<Helth>(world, pos));
		break;

	case 2:
		gameObjects.emplace_back(std::make_shared<ToiletPaper>(world, pos));
		break;

	case 3:
		gameObjects.emplace_back(std::make_shared<Virus>(world, pos));
		break;

	default:
		break;
	}

}
