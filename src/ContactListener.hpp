#pragma once

#include <Box2D/Box2D.h>

class ContactListener : public b2ContactListener {
public:
	void BeginContact(b2Contact*) override;
	void EndContact(b2Contact*) override;
};
