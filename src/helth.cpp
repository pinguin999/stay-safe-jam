#include "helth.hpp"
#include "constants.hpp"
#include "Player.hpp"

Helth::Helth(b2World& world, const jngl::Vec2 position) {
	b2BodyDef bodyDef;
	bodyDef.type = b2_staticBody;
	bodyDef.position = pixelToMeter(position);
	body = world.CreateBody(&bodyDef);
	body->SetLinearDamping(10.f);
	body->SetAngularDamping(10);

	body->SetUserData(static_cast<GameObject*>(this));

	b2CircleShape shape = b2CircleShape();
	shape.m_radius = pixelToMeter(25);
	createFixtureFromShape(shape);

}

Helth::~Helth() {
	if (body) {
		body->GetWorld()->DestroyBody(body);
	}
}

bool Helth::step() {
	checkOutOfScreen();
	return destroy;
}

void Helth::draw() const {
	jngl::pushMatrix();
	const auto transform = body->GetTransform();
	jngl::translate(meterToPixel(transform.p));
	jngl::rotate(transform.q.GetAngle() * 180 / M_PI);
	sprite.draw();
	jngl::popMatrix();
}

void Helth::onContact(GameObject* other){
	if (const auto player = dynamic_cast<Player*>(other)) {
		player->heal();
		destroy = true;
	}
}

