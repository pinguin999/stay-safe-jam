#include "GameObject.hpp"
#include <Box2D/Box2D.h>
#include <jngl.hpp>

class Virus : public GameObject {
public:
	Virus(b2World& world, jngl::Vec2 position);
	~Virus();


	void draw() const override;
	bool step() override;
    void onContact(GameObject*) override;

private:
	jngl::Sprite sprite{"virus"};
    bool destroy = false;
};
